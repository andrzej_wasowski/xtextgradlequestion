package org.xtext.example.mydsl.parser.antlr.internal;

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import org.xtext.example.mydsl.services.MyDslGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalMyDslParser extends AbstractInternalAntlrParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_STRING", "RULE_ID", "RULE_INT", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'FiniteStateMachine'", "'{'", "'initial'", "'states'", "','", "'}'", "'State'", "'leavingTransitions'", "'Transition'", "'input'", "'output'", "'target'"
    };
    public static final int RULE_STRING=4;
    public static final int RULE_SL_COMMENT=8;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__11=11;
    public static final int T__12=12;
    public static final int T__13=13;
    public static final int T__14=14;
    public static final int EOF=-1;
    public static final int RULE_ID=5;
    public static final int RULE_WS=9;
    public static final int RULE_ANY_OTHER=10;
    public static final int RULE_INT=6;
    public static final int T__22=22;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__20=20;
    public static final int T__21=21;

    // delegates
    // delegators


        public InternalMyDslParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalMyDslParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalMyDslParser.tokenNames; }
    public String getGrammarFileName() { return "InternalMyDsl.g"; }



     	private MyDslGrammarAccess grammarAccess;

        public InternalMyDslParser(TokenStream input, MyDslGrammarAccess grammarAccess) {
            this(input);
            this.grammarAccess = grammarAccess;
            registerRules(grammarAccess.getGrammar());
        }

        @Override
        protected String getFirstRuleName() {
        	return "FiniteStateMachine";
       	}

       	@Override
       	protected MyDslGrammarAccess getGrammarAccess() {
       		return grammarAccess;
       	}




    // $ANTLR start "entryRuleFiniteStateMachine"
    // InternalMyDsl.g:64:1: entryRuleFiniteStateMachine returns [EObject current=null] : iv_ruleFiniteStateMachine= ruleFiniteStateMachine EOF ;
    public final EObject entryRuleFiniteStateMachine() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleFiniteStateMachine = null;


        try {
            // InternalMyDsl.g:64:59: (iv_ruleFiniteStateMachine= ruleFiniteStateMachine EOF )
            // InternalMyDsl.g:65:2: iv_ruleFiniteStateMachine= ruleFiniteStateMachine EOF
            {
             newCompositeNode(grammarAccess.getFiniteStateMachineRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleFiniteStateMachine=ruleFiniteStateMachine();

            state._fsp--;

             current =iv_ruleFiniteStateMachine; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleFiniteStateMachine"


    // $ANTLR start "ruleFiniteStateMachine"
    // InternalMyDsl.g:71:1: ruleFiniteStateMachine returns [EObject current=null] : (otherlv_0= 'FiniteStateMachine' ( (lv_name_1_0= ruleEString ) ) otherlv_2= '{' otherlv_3= 'initial' ( ( ruleEString ) ) otherlv_5= 'states' otherlv_6= '{' ( (lv_states_7_0= ruleState ) ) (otherlv_8= ',' ( (lv_states_9_0= ruleState ) ) )* otherlv_10= '}' otherlv_11= '}' ) ;
    public final EObject ruleFiniteStateMachine() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        Token otherlv_6=null;
        Token otherlv_8=null;
        Token otherlv_10=null;
        Token otherlv_11=null;
        AntlrDatatypeRuleToken lv_name_1_0 = null;

        EObject lv_states_7_0 = null;

        EObject lv_states_9_0 = null;



        	enterRule();

        try {
            // InternalMyDsl.g:77:2: ( (otherlv_0= 'FiniteStateMachine' ( (lv_name_1_0= ruleEString ) ) otherlv_2= '{' otherlv_3= 'initial' ( ( ruleEString ) ) otherlv_5= 'states' otherlv_6= '{' ( (lv_states_7_0= ruleState ) ) (otherlv_8= ',' ( (lv_states_9_0= ruleState ) ) )* otherlv_10= '}' otherlv_11= '}' ) )
            // InternalMyDsl.g:78:2: (otherlv_0= 'FiniteStateMachine' ( (lv_name_1_0= ruleEString ) ) otherlv_2= '{' otherlv_3= 'initial' ( ( ruleEString ) ) otherlv_5= 'states' otherlv_6= '{' ( (lv_states_7_0= ruleState ) ) (otherlv_8= ',' ( (lv_states_9_0= ruleState ) ) )* otherlv_10= '}' otherlv_11= '}' )
            {
            // InternalMyDsl.g:78:2: (otherlv_0= 'FiniteStateMachine' ( (lv_name_1_0= ruleEString ) ) otherlv_2= '{' otherlv_3= 'initial' ( ( ruleEString ) ) otherlv_5= 'states' otherlv_6= '{' ( (lv_states_7_0= ruleState ) ) (otherlv_8= ',' ( (lv_states_9_0= ruleState ) ) )* otherlv_10= '}' otherlv_11= '}' )
            // InternalMyDsl.g:79:3: otherlv_0= 'FiniteStateMachine' ( (lv_name_1_0= ruleEString ) ) otherlv_2= '{' otherlv_3= 'initial' ( ( ruleEString ) ) otherlv_5= 'states' otherlv_6= '{' ( (lv_states_7_0= ruleState ) ) (otherlv_8= ',' ( (lv_states_9_0= ruleState ) ) )* otherlv_10= '}' otherlv_11= '}'
            {
            otherlv_0=(Token)match(input,11,FOLLOW_3); 

            			newLeafNode(otherlv_0, grammarAccess.getFiniteStateMachineAccess().getFiniteStateMachineKeyword_0());
            		
            // InternalMyDsl.g:83:3: ( (lv_name_1_0= ruleEString ) )
            // InternalMyDsl.g:84:4: (lv_name_1_0= ruleEString )
            {
            // InternalMyDsl.g:84:4: (lv_name_1_0= ruleEString )
            // InternalMyDsl.g:85:5: lv_name_1_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getFiniteStateMachineAccess().getNameEStringParserRuleCall_1_0());
            				
            pushFollow(FOLLOW_4);
            lv_name_1_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getFiniteStateMachineRule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_1_0,
            						"org.xtext.example.mydsl.MyDsl.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_2=(Token)match(input,12,FOLLOW_5); 

            			newLeafNode(otherlv_2, grammarAccess.getFiniteStateMachineAccess().getLeftCurlyBracketKeyword_2());
            		
            otherlv_3=(Token)match(input,13,FOLLOW_3); 

            			newLeafNode(otherlv_3, grammarAccess.getFiniteStateMachineAccess().getInitialKeyword_3());
            		
            // InternalMyDsl.g:110:3: ( ( ruleEString ) )
            // InternalMyDsl.g:111:4: ( ruleEString )
            {
            // InternalMyDsl.g:111:4: ( ruleEString )
            // InternalMyDsl.g:112:5: ruleEString
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getFiniteStateMachineRule());
            					}
            				

            					newCompositeNode(grammarAccess.getFiniteStateMachineAccess().getInitialStateCrossReference_4_0());
            				
            pushFollow(FOLLOW_6);
            ruleEString();

            state._fsp--;


            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_5=(Token)match(input,14,FOLLOW_4); 

            			newLeafNode(otherlv_5, grammarAccess.getFiniteStateMachineAccess().getStatesKeyword_5());
            		
            otherlv_6=(Token)match(input,12,FOLLOW_7); 

            			newLeafNode(otherlv_6, grammarAccess.getFiniteStateMachineAccess().getLeftCurlyBracketKeyword_6());
            		
            // InternalMyDsl.g:134:3: ( (lv_states_7_0= ruleState ) )
            // InternalMyDsl.g:135:4: (lv_states_7_0= ruleState )
            {
            // InternalMyDsl.g:135:4: (lv_states_7_0= ruleState )
            // InternalMyDsl.g:136:5: lv_states_7_0= ruleState
            {

            					newCompositeNode(grammarAccess.getFiniteStateMachineAccess().getStatesStateParserRuleCall_7_0());
            				
            pushFollow(FOLLOW_8);
            lv_states_7_0=ruleState();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getFiniteStateMachineRule());
            					}
            					add(
            						current,
            						"states",
            						lv_states_7_0,
            						"org.xtext.example.mydsl.MyDsl.State");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalMyDsl.g:153:3: (otherlv_8= ',' ( (lv_states_9_0= ruleState ) ) )*
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( (LA1_0==15) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // InternalMyDsl.g:154:4: otherlv_8= ',' ( (lv_states_9_0= ruleState ) )
            	    {
            	    otherlv_8=(Token)match(input,15,FOLLOW_7); 

            	    				newLeafNode(otherlv_8, grammarAccess.getFiniteStateMachineAccess().getCommaKeyword_8_0());
            	    			
            	    // InternalMyDsl.g:158:4: ( (lv_states_9_0= ruleState ) )
            	    // InternalMyDsl.g:159:5: (lv_states_9_0= ruleState )
            	    {
            	    // InternalMyDsl.g:159:5: (lv_states_9_0= ruleState )
            	    // InternalMyDsl.g:160:6: lv_states_9_0= ruleState
            	    {

            	    						newCompositeNode(grammarAccess.getFiniteStateMachineAccess().getStatesStateParserRuleCall_8_1_0());
            	    					
            	    pushFollow(FOLLOW_8);
            	    lv_states_9_0=ruleState();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getFiniteStateMachineRule());
            	    						}
            	    						add(
            	    							current,
            	    							"states",
            	    							lv_states_9_0,
            	    							"org.xtext.example.mydsl.MyDsl.State");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);

            otherlv_10=(Token)match(input,16,FOLLOW_9); 

            			newLeafNode(otherlv_10, grammarAccess.getFiniteStateMachineAccess().getRightCurlyBracketKeyword_9());
            		
            otherlv_11=(Token)match(input,16,FOLLOW_2); 

            			newLeafNode(otherlv_11, grammarAccess.getFiniteStateMachineAccess().getRightCurlyBracketKeyword_10());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleFiniteStateMachine"


    // $ANTLR start "entryRuleEString"
    // InternalMyDsl.g:190:1: entryRuleEString returns [String current=null] : iv_ruleEString= ruleEString EOF ;
    public final String entryRuleEString() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleEString = null;


        try {
            // InternalMyDsl.g:190:47: (iv_ruleEString= ruleEString EOF )
            // InternalMyDsl.g:191:2: iv_ruleEString= ruleEString EOF
            {
             newCompositeNode(grammarAccess.getEStringRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleEString=ruleEString();

            state._fsp--;

             current =iv_ruleEString.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleEString"


    // $ANTLR start "ruleEString"
    // InternalMyDsl.g:197:1: ruleEString returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (this_STRING_0= RULE_STRING | this_ID_1= RULE_ID ) ;
    public final AntlrDatatypeRuleToken ruleEString() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token this_STRING_0=null;
        Token this_ID_1=null;


        	enterRule();

        try {
            // InternalMyDsl.g:203:2: ( (this_STRING_0= RULE_STRING | this_ID_1= RULE_ID ) )
            // InternalMyDsl.g:204:2: (this_STRING_0= RULE_STRING | this_ID_1= RULE_ID )
            {
            // InternalMyDsl.g:204:2: (this_STRING_0= RULE_STRING | this_ID_1= RULE_ID )
            int alt2=2;
            int LA2_0 = input.LA(1);

            if ( (LA2_0==RULE_STRING) ) {
                alt2=1;
            }
            else if ( (LA2_0==RULE_ID) ) {
                alt2=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 2, 0, input);

                throw nvae;
            }
            switch (alt2) {
                case 1 :
                    // InternalMyDsl.g:205:3: this_STRING_0= RULE_STRING
                    {
                    this_STRING_0=(Token)match(input,RULE_STRING,FOLLOW_2); 

                    			current.merge(this_STRING_0);
                    		

                    			newLeafNode(this_STRING_0, grammarAccess.getEStringAccess().getSTRINGTerminalRuleCall_0());
                    		

                    }
                    break;
                case 2 :
                    // InternalMyDsl.g:213:3: this_ID_1= RULE_ID
                    {
                    this_ID_1=(Token)match(input,RULE_ID,FOLLOW_2); 

                    			current.merge(this_ID_1);
                    		

                    			newLeafNode(this_ID_1, grammarAccess.getEStringAccess().getIDTerminalRuleCall_1());
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEString"


    // $ANTLR start "entryRuleState"
    // InternalMyDsl.g:224:1: entryRuleState returns [EObject current=null] : iv_ruleState= ruleState EOF ;
    public final EObject entryRuleState() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleState = null;


        try {
            // InternalMyDsl.g:224:46: (iv_ruleState= ruleState EOF )
            // InternalMyDsl.g:225:2: iv_ruleState= ruleState EOF
            {
             newCompositeNode(grammarAccess.getStateRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleState=ruleState();

            state._fsp--;

             current =iv_ruleState; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleState"


    // $ANTLR start "ruleState"
    // InternalMyDsl.g:231:1: ruleState returns [EObject current=null] : ( () otherlv_1= 'State' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'leavingTransitions' otherlv_5= '{' ( (lv_leavingTransitions_6_0= ruleTransition ) ) (otherlv_7= ',' ( (lv_leavingTransitions_8_0= ruleTransition ) ) )* otherlv_9= '}' )? otherlv_10= '}' ) ;
    public final EObject ruleState() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_5=null;
        Token otherlv_7=null;
        Token otherlv_9=null;
        Token otherlv_10=null;
        AntlrDatatypeRuleToken lv_name_2_0 = null;

        EObject lv_leavingTransitions_6_0 = null;

        EObject lv_leavingTransitions_8_0 = null;



        	enterRule();

        try {
            // InternalMyDsl.g:237:2: ( ( () otherlv_1= 'State' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'leavingTransitions' otherlv_5= '{' ( (lv_leavingTransitions_6_0= ruleTransition ) ) (otherlv_7= ',' ( (lv_leavingTransitions_8_0= ruleTransition ) ) )* otherlv_9= '}' )? otherlv_10= '}' ) )
            // InternalMyDsl.g:238:2: ( () otherlv_1= 'State' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'leavingTransitions' otherlv_5= '{' ( (lv_leavingTransitions_6_0= ruleTransition ) ) (otherlv_7= ',' ( (lv_leavingTransitions_8_0= ruleTransition ) ) )* otherlv_9= '}' )? otherlv_10= '}' )
            {
            // InternalMyDsl.g:238:2: ( () otherlv_1= 'State' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'leavingTransitions' otherlv_5= '{' ( (lv_leavingTransitions_6_0= ruleTransition ) ) (otherlv_7= ',' ( (lv_leavingTransitions_8_0= ruleTransition ) ) )* otherlv_9= '}' )? otherlv_10= '}' )
            // InternalMyDsl.g:239:3: () otherlv_1= 'State' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'leavingTransitions' otherlv_5= '{' ( (lv_leavingTransitions_6_0= ruleTransition ) ) (otherlv_7= ',' ( (lv_leavingTransitions_8_0= ruleTransition ) ) )* otherlv_9= '}' )? otherlv_10= '}'
            {
            // InternalMyDsl.g:239:3: ()
            // InternalMyDsl.g:240:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getStateAccess().getStateAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,17,FOLLOW_3); 

            			newLeafNode(otherlv_1, grammarAccess.getStateAccess().getStateKeyword_1());
            		
            // InternalMyDsl.g:250:3: ( (lv_name_2_0= ruleEString ) )
            // InternalMyDsl.g:251:4: (lv_name_2_0= ruleEString )
            {
            // InternalMyDsl.g:251:4: (lv_name_2_0= ruleEString )
            // InternalMyDsl.g:252:5: lv_name_2_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getStateAccess().getNameEStringParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_4);
            lv_name_2_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getStateRule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_2_0,
            						"org.xtext.example.mydsl.MyDsl.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_3=(Token)match(input,12,FOLLOW_10); 

            			newLeafNode(otherlv_3, grammarAccess.getStateAccess().getLeftCurlyBracketKeyword_3());
            		
            // InternalMyDsl.g:273:3: (otherlv_4= 'leavingTransitions' otherlv_5= '{' ( (lv_leavingTransitions_6_0= ruleTransition ) ) (otherlv_7= ',' ( (lv_leavingTransitions_8_0= ruleTransition ) ) )* otherlv_9= '}' )?
            int alt4=2;
            int LA4_0 = input.LA(1);

            if ( (LA4_0==18) ) {
                alt4=1;
            }
            switch (alt4) {
                case 1 :
                    // InternalMyDsl.g:274:4: otherlv_4= 'leavingTransitions' otherlv_5= '{' ( (lv_leavingTransitions_6_0= ruleTransition ) ) (otherlv_7= ',' ( (lv_leavingTransitions_8_0= ruleTransition ) ) )* otherlv_9= '}'
                    {
                    otherlv_4=(Token)match(input,18,FOLLOW_4); 

                    				newLeafNode(otherlv_4, grammarAccess.getStateAccess().getLeavingTransitionsKeyword_4_0());
                    			
                    otherlv_5=(Token)match(input,12,FOLLOW_11); 

                    				newLeafNode(otherlv_5, grammarAccess.getStateAccess().getLeftCurlyBracketKeyword_4_1());
                    			
                    // InternalMyDsl.g:282:4: ( (lv_leavingTransitions_6_0= ruleTransition ) )
                    // InternalMyDsl.g:283:5: (lv_leavingTransitions_6_0= ruleTransition )
                    {
                    // InternalMyDsl.g:283:5: (lv_leavingTransitions_6_0= ruleTransition )
                    // InternalMyDsl.g:284:6: lv_leavingTransitions_6_0= ruleTransition
                    {

                    						newCompositeNode(grammarAccess.getStateAccess().getLeavingTransitionsTransitionParserRuleCall_4_2_0());
                    					
                    pushFollow(FOLLOW_8);
                    lv_leavingTransitions_6_0=ruleTransition();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getStateRule());
                    						}
                    						add(
                    							current,
                    							"leavingTransitions",
                    							lv_leavingTransitions_6_0,
                    							"org.xtext.example.mydsl.MyDsl.Transition");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalMyDsl.g:301:4: (otherlv_7= ',' ( (lv_leavingTransitions_8_0= ruleTransition ) ) )*
                    loop3:
                    do {
                        int alt3=2;
                        int LA3_0 = input.LA(1);

                        if ( (LA3_0==15) ) {
                            alt3=1;
                        }


                        switch (alt3) {
                    	case 1 :
                    	    // InternalMyDsl.g:302:5: otherlv_7= ',' ( (lv_leavingTransitions_8_0= ruleTransition ) )
                    	    {
                    	    otherlv_7=(Token)match(input,15,FOLLOW_11); 

                    	    					newLeafNode(otherlv_7, grammarAccess.getStateAccess().getCommaKeyword_4_3_0());
                    	    				
                    	    // InternalMyDsl.g:306:5: ( (lv_leavingTransitions_8_0= ruleTransition ) )
                    	    // InternalMyDsl.g:307:6: (lv_leavingTransitions_8_0= ruleTransition )
                    	    {
                    	    // InternalMyDsl.g:307:6: (lv_leavingTransitions_8_0= ruleTransition )
                    	    // InternalMyDsl.g:308:7: lv_leavingTransitions_8_0= ruleTransition
                    	    {

                    	    							newCompositeNode(grammarAccess.getStateAccess().getLeavingTransitionsTransitionParserRuleCall_4_3_1_0());
                    	    						
                    	    pushFollow(FOLLOW_8);
                    	    lv_leavingTransitions_8_0=ruleTransition();

                    	    state._fsp--;


                    	    							if (current==null) {
                    	    								current = createModelElementForParent(grammarAccess.getStateRule());
                    	    							}
                    	    							add(
                    	    								current,
                    	    								"leavingTransitions",
                    	    								lv_leavingTransitions_8_0,
                    	    								"org.xtext.example.mydsl.MyDsl.Transition");
                    	    							afterParserOrEnumRuleCall();
                    	    						

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop3;
                        }
                    } while (true);

                    otherlv_9=(Token)match(input,16,FOLLOW_9); 

                    				newLeafNode(otherlv_9, grammarAccess.getStateAccess().getRightCurlyBracketKeyword_4_4());
                    			

                    }
                    break;

            }

            otherlv_10=(Token)match(input,16,FOLLOW_2); 

            			newLeafNode(otherlv_10, grammarAccess.getStateAccess().getRightCurlyBracketKeyword_5());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleState"


    // $ANTLR start "entryRuleTransition"
    // InternalMyDsl.g:339:1: entryRuleTransition returns [EObject current=null] : iv_ruleTransition= ruleTransition EOF ;
    public final EObject entryRuleTransition() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleTransition = null;


        try {
            // InternalMyDsl.g:339:51: (iv_ruleTransition= ruleTransition EOF )
            // InternalMyDsl.g:340:2: iv_ruleTransition= ruleTransition EOF
            {
             newCompositeNode(grammarAccess.getTransitionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleTransition=ruleTransition();

            state._fsp--;

             current =iv_ruleTransition; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleTransition"


    // $ANTLR start "ruleTransition"
    // InternalMyDsl.g:346:1: ruleTransition returns [EObject current=null] : (otherlv_0= 'Transition' otherlv_1= '{' otherlv_2= 'input' ( (lv_input_3_0= ruleEString ) ) (otherlv_4= 'output' ( (lv_output_5_0= ruleEString ) ) )? otherlv_6= 'target' ( ( ruleEString ) ) otherlv_8= '}' ) ;
    public final EObject ruleTransition() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        Token otherlv_8=null;
        AntlrDatatypeRuleToken lv_input_3_0 = null;

        AntlrDatatypeRuleToken lv_output_5_0 = null;



        	enterRule();

        try {
            // InternalMyDsl.g:352:2: ( (otherlv_0= 'Transition' otherlv_1= '{' otherlv_2= 'input' ( (lv_input_3_0= ruleEString ) ) (otherlv_4= 'output' ( (lv_output_5_0= ruleEString ) ) )? otherlv_6= 'target' ( ( ruleEString ) ) otherlv_8= '}' ) )
            // InternalMyDsl.g:353:2: (otherlv_0= 'Transition' otherlv_1= '{' otherlv_2= 'input' ( (lv_input_3_0= ruleEString ) ) (otherlv_4= 'output' ( (lv_output_5_0= ruleEString ) ) )? otherlv_6= 'target' ( ( ruleEString ) ) otherlv_8= '}' )
            {
            // InternalMyDsl.g:353:2: (otherlv_0= 'Transition' otherlv_1= '{' otherlv_2= 'input' ( (lv_input_3_0= ruleEString ) ) (otherlv_4= 'output' ( (lv_output_5_0= ruleEString ) ) )? otherlv_6= 'target' ( ( ruleEString ) ) otherlv_8= '}' )
            // InternalMyDsl.g:354:3: otherlv_0= 'Transition' otherlv_1= '{' otherlv_2= 'input' ( (lv_input_3_0= ruleEString ) ) (otherlv_4= 'output' ( (lv_output_5_0= ruleEString ) ) )? otherlv_6= 'target' ( ( ruleEString ) ) otherlv_8= '}'
            {
            otherlv_0=(Token)match(input,19,FOLLOW_4); 

            			newLeafNode(otherlv_0, grammarAccess.getTransitionAccess().getTransitionKeyword_0());
            		
            otherlv_1=(Token)match(input,12,FOLLOW_12); 

            			newLeafNode(otherlv_1, grammarAccess.getTransitionAccess().getLeftCurlyBracketKeyword_1());
            		
            otherlv_2=(Token)match(input,20,FOLLOW_3); 

            			newLeafNode(otherlv_2, grammarAccess.getTransitionAccess().getInputKeyword_2());
            		
            // InternalMyDsl.g:366:3: ( (lv_input_3_0= ruleEString ) )
            // InternalMyDsl.g:367:4: (lv_input_3_0= ruleEString )
            {
            // InternalMyDsl.g:367:4: (lv_input_3_0= ruleEString )
            // InternalMyDsl.g:368:5: lv_input_3_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getTransitionAccess().getInputEStringParserRuleCall_3_0());
            				
            pushFollow(FOLLOW_13);
            lv_input_3_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getTransitionRule());
            					}
            					set(
            						current,
            						"input",
            						lv_input_3_0,
            						"org.xtext.example.mydsl.MyDsl.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalMyDsl.g:385:3: (otherlv_4= 'output' ( (lv_output_5_0= ruleEString ) ) )?
            int alt5=2;
            int LA5_0 = input.LA(1);

            if ( (LA5_0==21) ) {
                alt5=1;
            }
            switch (alt5) {
                case 1 :
                    // InternalMyDsl.g:386:4: otherlv_4= 'output' ( (lv_output_5_0= ruleEString ) )
                    {
                    otherlv_4=(Token)match(input,21,FOLLOW_3); 

                    				newLeafNode(otherlv_4, grammarAccess.getTransitionAccess().getOutputKeyword_4_0());
                    			
                    // InternalMyDsl.g:390:4: ( (lv_output_5_0= ruleEString ) )
                    // InternalMyDsl.g:391:5: (lv_output_5_0= ruleEString )
                    {
                    // InternalMyDsl.g:391:5: (lv_output_5_0= ruleEString )
                    // InternalMyDsl.g:392:6: lv_output_5_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getTransitionAccess().getOutputEStringParserRuleCall_4_1_0());
                    					
                    pushFollow(FOLLOW_14);
                    lv_output_5_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getTransitionRule());
                    						}
                    						set(
                    							current,
                    							"output",
                    							lv_output_5_0,
                    							"org.xtext.example.mydsl.MyDsl.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            otherlv_6=(Token)match(input,22,FOLLOW_3); 

            			newLeafNode(otherlv_6, grammarAccess.getTransitionAccess().getTargetKeyword_5());
            		
            // InternalMyDsl.g:414:3: ( ( ruleEString ) )
            // InternalMyDsl.g:415:4: ( ruleEString )
            {
            // InternalMyDsl.g:415:4: ( ruleEString )
            // InternalMyDsl.g:416:5: ruleEString
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getTransitionRule());
            					}
            				

            					newCompositeNode(grammarAccess.getTransitionAccess().getTargetStateCrossReference_6_0());
            				
            pushFollow(FOLLOW_9);
            ruleEString();

            state._fsp--;


            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_8=(Token)match(input,16,FOLLOW_2); 

            			newLeafNode(otherlv_8, grammarAccess.getTransitionAccess().getRightCurlyBracketKeyword_7());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleTransition"

    // Delegated rules


 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x0000000000000030L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000000001000L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x0000000000002000L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x0000000000004000L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x0000000000020000L});
    public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x0000000000018000L});
    public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x0000000000010000L});
    public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x0000000000050000L});
    public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x0000000000080000L});
    public static final BitSet FOLLOW_12 = new BitSet(new long[]{0x0000000000100000L});
    public static final BitSet FOLLOW_13 = new BitSet(new long[]{0x0000000000600000L});
    public static final BitSet FOLLOW_14 = new BitSet(new long[]{0x0000000000400000L});

}
package org.xtext.example.mydsl.idea.parser.antlr.internal;

import org.eclipse.xtext.idea.parser.AbstractPsiAntlrParser;
import org.xtext.example.mydsl.idea.lang.MyDslElementTypeProvider;
import org.eclipse.xtext.idea.parser.TokenTypeProvider;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.xtext.example.mydsl.services.MyDslGrammarAccess;

import com.intellij.lang.PsiBuilder;


import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class PsiInternalMyDslParser extends AbstractPsiAntlrParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_STRING", "RULE_ID", "RULE_INT", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'FiniteStateMachine'", "'{'", "'initial'", "'states'", "','", "'}'", "'State'", "'leavingTransitions'", "'Transition'", "'input'", "'output'", "'target'"
    };
    public static final int RULE_STRING=4;
    public static final int RULE_SL_COMMENT=8;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__11=11;
    public static final int T__12=12;
    public static final int T__13=13;
    public static final int T__14=14;
    public static final int EOF=-1;
    public static final int RULE_ID=5;
    public static final int RULE_WS=9;
    public static final int RULE_ANY_OTHER=10;
    public static final int RULE_INT=6;
    public static final int T__22=22;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__20=20;
    public static final int T__21=21;

    // delegates
    // delegators


        public PsiInternalMyDslParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public PsiInternalMyDslParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return PsiInternalMyDslParser.tokenNames; }
    public String getGrammarFileName() { return "PsiInternalMyDsl.g"; }



    	protected MyDslGrammarAccess grammarAccess;

    	protected MyDslElementTypeProvider elementTypeProvider;

    	public PsiInternalMyDslParser(PsiBuilder builder, TokenStream input, MyDslElementTypeProvider elementTypeProvider, MyDslGrammarAccess grammarAccess) {
    		this(input);
    		setPsiBuilder(builder);
    		this.grammarAccess = grammarAccess;
    		this.elementTypeProvider = elementTypeProvider;
    	}

    	@Override
    	protected String getFirstRuleName() {
    		return "FiniteStateMachine";
    	}




    // $ANTLR start "entryRuleFiniteStateMachine"
    // PsiInternalMyDsl.g:52:1: entryRuleFiniteStateMachine returns [Boolean current=false] : iv_ruleFiniteStateMachine= ruleFiniteStateMachine EOF ;
    public final Boolean entryRuleFiniteStateMachine() throws RecognitionException {
        Boolean current = false;

        Boolean iv_ruleFiniteStateMachine = null;


        try {
            // PsiInternalMyDsl.g:52:60: (iv_ruleFiniteStateMachine= ruleFiniteStateMachine EOF )
            // PsiInternalMyDsl.g:53:2: iv_ruleFiniteStateMachine= ruleFiniteStateMachine EOF
            {
             markComposite(elementTypeProvider.getFiniteStateMachineElementType()); 
            pushFollow(FOLLOW_1);
            iv_ruleFiniteStateMachine=ruleFiniteStateMachine();

            state._fsp--;

             current =iv_ruleFiniteStateMachine; 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleFiniteStateMachine"


    // $ANTLR start "ruleFiniteStateMachine"
    // PsiInternalMyDsl.g:59:1: ruleFiniteStateMachine returns [Boolean current=false] : (otherlv_0= 'FiniteStateMachine' ( (lv_name_1_0= ruleEString ) ) otherlv_2= '{' otherlv_3= 'initial' ( ( ruleEString ) ) otherlv_5= 'states' otherlv_6= '{' ( (lv_states_7_0= ruleState ) ) (otherlv_8= ',' ( (lv_states_9_0= ruleState ) ) )* otherlv_10= '}' otherlv_11= '}' ) ;
    public final Boolean ruleFiniteStateMachine() throws RecognitionException {
        Boolean current = false;

        Token otherlv_0=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        Token otherlv_6=null;
        Token otherlv_8=null;
        Token otherlv_10=null;
        Token otherlv_11=null;
        Boolean lv_name_1_0 = null;

        Boolean lv_states_7_0 = null;

        Boolean lv_states_9_0 = null;


        try {
            // PsiInternalMyDsl.g:60:1: ( (otherlv_0= 'FiniteStateMachine' ( (lv_name_1_0= ruleEString ) ) otherlv_2= '{' otherlv_3= 'initial' ( ( ruleEString ) ) otherlv_5= 'states' otherlv_6= '{' ( (lv_states_7_0= ruleState ) ) (otherlv_8= ',' ( (lv_states_9_0= ruleState ) ) )* otherlv_10= '}' otherlv_11= '}' ) )
            // PsiInternalMyDsl.g:61:2: (otherlv_0= 'FiniteStateMachine' ( (lv_name_1_0= ruleEString ) ) otherlv_2= '{' otherlv_3= 'initial' ( ( ruleEString ) ) otherlv_5= 'states' otherlv_6= '{' ( (lv_states_7_0= ruleState ) ) (otherlv_8= ',' ( (lv_states_9_0= ruleState ) ) )* otherlv_10= '}' otherlv_11= '}' )
            {
            // PsiInternalMyDsl.g:61:2: (otherlv_0= 'FiniteStateMachine' ( (lv_name_1_0= ruleEString ) ) otherlv_2= '{' otherlv_3= 'initial' ( ( ruleEString ) ) otherlv_5= 'states' otherlv_6= '{' ( (lv_states_7_0= ruleState ) ) (otherlv_8= ',' ( (lv_states_9_0= ruleState ) ) )* otherlv_10= '}' otherlv_11= '}' )
            // PsiInternalMyDsl.g:62:3: otherlv_0= 'FiniteStateMachine' ( (lv_name_1_0= ruleEString ) ) otherlv_2= '{' otherlv_3= 'initial' ( ( ruleEString ) ) otherlv_5= 'states' otherlv_6= '{' ( (lv_states_7_0= ruleState ) ) (otherlv_8= ',' ( (lv_states_9_0= ruleState ) ) )* otherlv_10= '}' otherlv_11= '}'
            {

            			markLeaf(elementTypeProvider.getFiniteStateMachine_FiniteStateMachineKeyword_0ElementType());
            		
            otherlv_0=(Token)match(input,11,FOLLOW_3); 

            			doneLeaf(otherlv_0);
            		
            // PsiInternalMyDsl.g:69:3: ( (lv_name_1_0= ruleEString ) )
            // PsiInternalMyDsl.g:70:4: (lv_name_1_0= ruleEString )
            {
            // PsiInternalMyDsl.g:70:4: (lv_name_1_0= ruleEString )
            // PsiInternalMyDsl.g:71:5: lv_name_1_0= ruleEString
            {

            					markComposite(elementTypeProvider.getFiniteStateMachine_NameEStringParserRuleCall_1_0ElementType());
            				
            pushFollow(FOLLOW_4);
            lv_name_1_0=ruleEString();

            state._fsp--;


            					doneComposite();
            					if(!current) {
            						associateWithSemanticElement();
            						current = true;
            					}
            				

            }


            }


            			markLeaf(elementTypeProvider.getFiniteStateMachine_LeftCurlyBracketKeyword_2ElementType());
            		
            otherlv_2=(Token)match(input,12,FOLLOW_5); 

            			doneLeaf(otherlv_2);
            		

            			markLeaf(elementTypeProvider.getFiniteStateMachine_InitialKeyword_3ElementType());
            		
            otherlv_3=(Token)match(input,13,FOLLOW_3); 

            			doneLeaf(otherlv_3);
            		
            // PsiInternalMyDsl.g:98:3: ( ( ruleEString ) )
            // PsiInternalMyDsl.g:99:4: ( ruleEString )
            {
            // PsiInternalMyDsl.g:99:4: ( ruleEString )
            // PsiInternalMyDsl.g:100:5: ruleEString
            {

            					if (!current) {
            						associateWithSemanticElement();
            						current = true;
            					}
            				

            					markComposite(elementTypeProvider.getFiniteStateMachine_InitialStateCrossReference_4_0ElementType());
            				
            pushFollow(FOLLOW_6);
            ruleEString();

            state._fsp--;


            					doneComposite();
            				

            }


            }


            			markLeaf(elementTypeProvider.getFiniteStateMachine_StatesKeyword_5ElementType());
            		
            otherlv_5=(Token)match(input,14,FOLLOW_4); 

            			doneLeaf(otherlv_5);
            		

            			markLeaf(elementTypeProvider.getFiniteStateMachine_LeftCurlyBracketKeyword_6ElementType());
            		
            otherlv_6=(Token)match(input,12,FOLLOW_7); 

            			doneLeaf(otherlv_6);
            		
            // PsiInternalMyDsl.g:129:3: ( (lv_states_7_0= ruleState ) )
            // PsiInternalMyDsl.g:130:4: (lv_states_7_0= ruleState )
            {
            // PsiInternalMyDsl.g:130:4: (lv_states_7_0= ruleState )
            // PsiInternalMyDsl.g:131:5: lv_states_7_0= ruleState
            {

            					markComposite(elementTypeProvider.getFiniteStateMachine_StatesStateParserRuleCall_7_0ElementType());
            				
            pushFollow(FOLLOW_8);
            lv_states_7_0=ruleState();

            state._fsp--;


            					doneComposite();
            					if(!current) {
            						associateWithSemanticElement();
            						current = true;
            					}
            				

            }


            }

            // PsiInternalMyDsl.g:144:3: (otherlv_8= ',' ( (lv_states_9_0= ruleState ) ) )*
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( (LA1_0==15) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // PsiInternalMyDsl.g:145:4: otherlv_8= ',' ( (lv_states_9_0= ruleState ) )
            	    {

            	    				markLeaf(elementTypeProvider.getFiniteStateMachine_CommaKeyword_8_0ElementType());
            	    			
            	    otherlv_8=(Token)match(input,15,FOLLOW_7); 

            	    				doneLeaf(otherlv_8);
            	    			
            	    // PsiInternalMyDsl.g:152:4: ( (lv_states_9_0= ruleState ) )
            	    // PsiInternalMyDsl.g:153:5: (lv_states_9_0= ruleState )
            	    {
            	    // PsiInternalMyDsl.g:153:5: (lv_states_9_0= ruleState )
            	    // PsiInternalMyDsl.g:154:6: lv_states_9_0= ruleState
            	    {

            	    						markComposite(elementTypeProvider.getFiniteStateMachine_StatesStateParserRuleCall_8_1_0ElementType());
            	    					
            	    pushFollow(FOLLOW_8);
            	    lv_states_9_0=ruleState();

            	    state._fsp--;


            	    						doneComposite();
            	    						if(!current) {
            	    							associateWithSemanticElement();
            	    							current = true;
            	    						}
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);


            			markLeaf(elementTypeProvider.getFiniteStateMachine_RightCurlyBracketKeyword_9ElementType());
            		
            otherlv_10=(Token)match(input,16,FOLLOW_9); 

            			doneLeaf(otherlv_10);
            		

            			markLeaf(elementTypeProvider.getFiniteStateMachine_RightCurlyBracketKeyword_10ElementType());
            		
            otherlv_11=(Token)match(input,16,FOLLOW_2); 

            			doneLeaf(otherlv_11);
            		

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleFiniteStateMachine"


    // $ANTLR start "entryRuleEString"
    // PsiInternalMyDsl.g:186:1: entryRuleEString returns [Boolean current=false] : iv_ruleEString= ruleEString EOF ;
    public final Boolean entryRuleEString() throws RecognitionException {
        Boolean current = false;

        Boolean iv_ruleEString = null;


        try {
            // PsiInternalMyDsl.g:186:49: (iv_ruleEString= ruleEString EOF )
            // PsiInternalMyDsl.g:187:2: iv_ruleEString= ruleEString EOF
            {
             markComposite(elementTypeProvider.getEStringElementType()); 
            pushFollow(FOLLOW_1);
            iv_ruleEString=ruleEString();

            state._fsp--;

             current =iv_ruleEString; 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleEString"


    // $ANTLR start "ruleEString"
    // PsiInternalMyDsl.g:193:1: ruleEString returns [Boolean current=false] : (this_STRING_0= RULE_STRING | this_ID_1= RULE_ID ) ;
    public final Boolean ruleEString() throws RecognitionException {
        Boolean current = false;

        Token this_STRING_0=null;
        Token this_ID_1=null;

        try {
            // PsiInternalMyDsl.g:194:1: ( (this_STRING_0= RULE_STRING | this_ID_1= RULE_ID ) )
            // PsiInternalMyDsl.g:195:2: (this_STRING_0= RULE_STRING | this_ID_1= RULE_ID )
            {
            // PsiInternalMyDsl.g:195:2: (this_STRING_0= RULE_STRING | this_ID_1= RULE_ID )
            int alt2=2;
            int LA2_0 = input.LA(1);

            if ( (LA2_0==RULE_STRING) ) {
                alt2=1;
            }
            else if ( (LA2_0==RULE_ID) ) {
                alt2=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 2, 0, input);

                throw nvae;
            }
            switch (alt2) {
                case 1 :
                    // PsiInternalMyDsl.g:196:3: this_STRING_0= RULE_STRING
                    {

                    			markLeaf(elementTypeProvider.getEString_STRINGTerminalRuleCall_0ElementType());
                    		
                    this_STRING_0=(Token)match(input,RULE_STRING,FOLLOW_2); 

                    			doneLeaf(this_STRING_0);
                    		

                    }
                    break;
                case 2 :
                    // PsiInternalMyDsl.g:204:3: this_ID_1= RULE_ID
                    {

                    			markLeaf(elementTypeProvider.getEString_IDTerminalRuleCall_1ElementType());
                    		
                    this_ID_1=(Token)match(input,RULE_ID,FOLLOW_2); 

                    			doneLeaf(this_ID_1);
                    		

                    }
                    break;

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEString"


    // $ANTLR start "entryRuleState"
    // PsiInternalMyDsl.g:215:1: entryRuleState returns [Boolean current=false] : iv_ruleState= ruleState EOF ;
    public final Boolean entryRuleState() throws RecognitionException {
        Boolean current = false;

        Boolean iv_ruleState = null;


        try {
            // PsiInternalMyDsl.g:215:47: (iv_ruleState= ruleState EOF )
            // PsiInternalMyDsl.g:216:2: iv_ruleState= ruleState EOF
            {
             markComposite(elementTypeProvider.getStateElementType()); 
            pushFollow(FOLLOW_1);
            iv_ruleState=ruleState();

            state._fsp--;

             current =iv_ruleState; 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleState"


    // $ANTLR start "ruleState"
    // PsiInternalMyDsl.g:222:1: ruleState returns [Boolean current=false] : ( () otherlv_1= 'State' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'leavingTransitions' otherlv_5= '{' ( (lv_leavingTransitions_6_0= ruleTransition ) ) (otherlv_7= ',' ( (lv_leavingTransitions_8_0= ruleTransition ) ) )* otherlv_9= '}' )? otherlv_10= '}' ) ;
    public final Boolean ruleState() throws RecognitionException {
        Boolean current = false;

        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_5=null;
        Token otherlv_7=null;
        Token otherlv_9=null;
        Token otherlv_10=null;
        Boolean lv_name_2_0 = null;

        Boolean lv_leavingTransitions_6_0 = null;

        Boolean lv_leavingTransitions_8_0 = null;


        try {
            // PsiInternalMyDsl.g:223:1: ( ( () otherlv_1= 'State' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'leavingTransitions' otherlv_5= '{' ( (lv_leavingTransitions_6_0= ruleTransition ) ) (otherlv_7= ',' ( (lv_leavingTransitions_8_0= ruleTransition ) ) )* otherlv_9= '}' )? otherlv_10= '}' ) )
            // PsiInternalMyDsl.g:224:2: ( () otherlv_1= 'State' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'leavingTransitions' otherlv_5= '{' ( (lv_leavingTransitions_6_0= ruleTransition ) ) (otherlv_7= ',' ( (lv_leavingTransitions_8_0= ruleTransition ) ) )* otherlv_9= '}' )? otherlv_10= '}' )
            {
            // PsiInternalMyDsl.g:224:2: ( () otherlv_1= 'State' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'leavingTransitions' otherlv_5= '{' ( (lv_leavingTransitions_6_0= ruleTransition ) ) (otherlv_7= ',' ( (lv_leavingTransitions_8_0= ruleTransition ) ) )* otherlv_9= '}' )? otherlv_10= '}' )
            // PsiInternalMyDsl.g:225:3: () otherlv_1= 'State' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'leavingTransitions' otherlv_5= '{' ( (lv_leavingTransitions_6_0= ruleTransition ) ) (otherlv_7= ',' ( (lv_leavingTransitions_8_0= ruleTransition ) ) )* otherlv_9= '}' )? otherlv_10= '}'
            {
            // PsiInternalMyDsl.g:225:3: ()
            // PsiInternalMyDsl.g:226:4: 
            {

            				precedeComposite(elementTypeProvider.getState_StateAction_0ElementType());
            				doneComposite();
            				associateWithSemanticElement();
            			

            }


            			markLeaf(elementTypeProvider.getState_StateKeyword_1ElementType());
            		
            otherlv_1=(Token)match(input,17,FOLLOW_3); 

            			doneLeaf(otherlv_1);
            		
            // PsiInternalMyDsl.g:239:3: ( (lv_name_2_0= ruleEString ) )
            // PsiInternalMyDsl.g:240:4: (lv_name_2_0= ruleEString )
            {
            // PsiInternalMyDsl.g:240:4: (lv_name_2_0= ruleEString )
            // PsiInternalMyDsl.g:241:5: lv_name_2_0= ruleEString
            {

            					markComposite(elementTypeProvider.getState_NameEStringParserRuleCall_2_0ElementType());
            				
            pushFollow(FOLLOW_4);
            lv_name_2_0=ruleEString();

            state._fsp--;


            					doneComposite();
            					if(!current) {
            						associateWithSemanticElement();
            						current = true;
            					}
            				

            }


            }


            			markLeaf(elementTypeProvider.getState_LeftCurlyBracketKeyword_3ElementType());
            		
            otherlv_3=(Token)match(input,12,FOLLOW_10); 

            			doneLeaf(otherlv_3);
            		
            // PsiInternalMyDsl.g:261:3: (otherlv_4= 'leavingTransitions' otherlv_5= '{' ( (lv_leavingTransitions_6_0= ruleTransition ) ) (otherlv_7= ',' ( (lv_leavingTransitions_8_0= ruleTransition ) ) )* otherlv_9= '}' )?
            int alt4=2;
            int LA4_0 = input.LA(1);

            if ( (LA4_0==18) ) {
                alt4=1;
            }
            switch (alt4) {
                case 1 :
                    // PsiInternalMyDsl.g:262:4: otherlv_4= 'leavingTransitions' otherlv_5= '{' ( (lv_leavingTransitions_6_0= ruleTransition ) ) (otherlv_7= ',' ( (lv_leavingTransitions_8_0= ruleTransition ) ) )* otherlv_9= '}'
                    {

                    				markLeaf(elementTypeProvider.getState_LeavingTransitionsKeyword_4_0ElementType());
                    			
                    otherlv_4=(Token)match(input,18,FOLLOW_4); 

                    				doneLeaf(otherlv_4);
                    			

                    				markLeaf(elementTypeProvider.getState_LeftCurlyBracketKeyword_4_1ElementType());
                    			
                    otherlv_5=(Token)match(input,12,FOLLOW_11); 

                    				doneLeaf(otherlv_5);
                    			
                    // PsiInternalMyDsl.g:276:4: ( (lv_leavingTransitions_6_0= ruleTransition ) )
                    // PsiInternalMyDsl.g:277:5: (lv_leavingTransitions_6_0= ruleTransition )
                    {
                    // PsiInternalMyDsl.g:277:5: (lv_leavingTransitions_6_0= ruleTransition )
                    // PsiInternalMyDsl.g:278:6: lv_leavingTransitions_6_0= ruleTransition
                    {

                    						markComposite(elementTypeProvider.getState_LeavingTransitionsTransitionParserRuleCall_4_2_0ElementType());
                    					
                    pushFollow(FOLLOW_8);
                    lv_leavingTransitions_6_0=ruleTransition();

                    state._fsp--;


                    						doneComposite();
                    						if(!current) {
                    							associateWithSemanticElement();
                    							current = true;
                    						}
                    					

                    }


                    }

                    // PsiInternalMyDsl.g:291:4: (otherlv_7= ',' ( (lv_leavingTransitions_8_0= ruleTransition ) ) )*
                    loop3:
                    do {
                        int alt3=2;
                        int LA3_0 = input.LA(1);

                        if ( (LA3_0==15) ) {
                            alt3=1;
                        }


                        switch (alt3) {
                    	case 1 :
                    	    // PsiInternalMyDsl.g:292:5: otherlv_7= ',' ( (lv_leavingTransitions_8_0= ruleTransition ) )
                    	    {

                    	    					markLeaf(elementTypeProvider.getState_CommaKeyword_4_3_0ElementType());
                    	    				
                    	    otherlv_7=(Token)match(input,15,FOLLOW_11); 

                    	    					doneLeaf(otherlv_7);
                    	    				
                    	    // PsiInternalMyDsl.g:299:5: ( (lv_leavingTransitions_8_0= ruleTransition ) )
                    	    // PsiInternalMyDsl.g:300:6: (lv_leavingTransitions_8_0= ruleTransition )
                    	    {
                    	    // PsiInternalMyDsl.g:300:6: (lv_leavingTransitions_8_0= ruleTransition )
                    	    // PsiInternalMyDsl.g:301:7: lv_leavingTransitions_8_0= ruleTransition
                    	    {

                    	    							markComposite(elementTypeProvider.getState_LeavingTransitionsTransitionParserRuleCall_4_3_1_0ElementType());
                    	    						
                    	    pushFollow(FOLLOW_8);
                    	    lv_leavingTransitions_8_0=ruleTransition();

                    	    state._fsp--;


                    	    							doneComposite();
                    	    							if(!current) {
                    	    								associateWithSemanticElement();
                    	    								current = true;
                    	    							}
                    	    						

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop3;
                        }
                    } while (true);


                    				markLeaf(elementTypeProvider.getState_RightCurlyBracketKeyword_4_4ElementType());
                    			
                    otherlv_9=(Token)match(input,16,FOLLOW_9); 

                    				doneLeaf(otherlv_9);
                    			

                    }
                    break;

            }


            			markLeaf(elementTypeProvider.getState_RightCurlyBracketKeyword_5ElementType());
            		
            otherlv_10=(Token)match(input,16,FOLLOW_2); 

            			doneLeaf(otherlv_10);
            		

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleState"


    // $ANTLR start "entryRuleTransition"
    // PsiInternalMyDsl.g:334:1: entryRuleTransition returns [Boolean current=false] : iv_ruleTransition= ruleTransition EOF ;
    public final Boolean entryRuleTransition() throws RecognitionException {
        Boolean current = false;

        Boolean iv_ruleTransition = null;


        try {
            // PsiInternalMyDsl.g:334:52: (iv_ruleTransition= ruleTransition EOF )
            // PsiInternalMyDsl.g:335:2: iv_ruleTransition= ruleTransition EOF
            {
             markComposite(elementTypeProvider.getTransitionElementType()); 
            pushFollow(FOLLOW_1);
            iv_ruleTransition=ruleTransition();

            state._fsp--;

             current =iv_ruleTransition; 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleTransition"


    // $ANTLR start "ruleTransition"
    // PsiInternalMyDsl.g:341:1: ruleTransition returns [Boolean current=false] : (otherlv_0= 'Transition' otherlv_1= '{' otherlv_2= 'input' ( (lv_input_3_0= ruleEString ) ) (otherlv_4= 'output' ( (lv_output_5_0= ruleEString ) ) )? otherlv_6= 'target' ( ( ruleEString ) ) otherlv_8= '}' ) ;
    public final Boolean ruleTransition() throws RecognitionException {
        Boolean current = false;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        Token otherlv_8=null;
        Boolean lv_input_3_0 = null;

        Boolean lv_output_5_0 = null;


        try {
            // PsiInternalMyDsl.g:342:1: ( (otherlv_0= 'Transition' otherlv_1= '{' otherlv_2= 'input' ( (lv_input_3_0= ruleEString ) ) (otherlv_4= 'output' ( (lv_output_5_0= ruleEString ) ) )? otherlv_6= 'target' ( ( ruleEString ) ) otherlv_8= '}' ) )
            // PsiInternalMyDsl.g:343:2: (otherlv_0= 'Transition' otherlv_1= '{' otherlv_2= 'input' ( (lv_input_3_0= ruleEString ) ) (otherlv_4= 'output' ( (lv_output_5_0= ruleEString ) ) )? otherlv_6= 'target' ( ( ruleEString ) ) otherlv_8= '}' )
            {
            // PsiInternalMyDsl.g:343:2: (otherlv_0= 'Transition' otherlv_1= '{' otherlv_2= 'input' ( (lv_input_3_0= ruleEString ) ) (otherlv_4= 'output' ( (lv_output_5_0= ruleEString ) ) )? otherlv_6= 'target' ( ( ruleEString ) ) otherlv_8= '}' )
            // PsiInternalMyDsl.g:344:3: otherlv_0= 'Transition' otherlv_1= '{' otherlv_2= 'input' ( (lv_input_3_0= ruleEString ) ) (otherlv_4= 'output' ( (lv_output_5_0= ruleEString ) ) )? otherlv_6= 'target' ( ( ruleEString ) ) otherlv_8= '}'
            {

            			markLeaf(elementTypeProvider.getTransition_TransitionKeyword_0ElementType());
            		
            otherlv_0=(Token)match(input,19,FOLLOW_4); 

            			doneLeaf(otherlv_0);
            		

            			markLeaf(elementTypeProvider.getTransition_LeftCurlyBracketKeyword_1ElementType());
            		
            otherlv_1=(Token)match(input,12,FOLLOW_12); 

            			doneLeaf(otherlv_1);
            		

            			markLeaf(elementTypeProvider.getTransition_InputKeyword_2ElementType());
            		
            otherlv_2=(Token)match(input,20,FOLLOW_3); 

            			doneLeaf(otherlv_2);
            		
            // PsiInternalMyDsl.g:365:3: ( (lv_input_3_0= ruleEString ) )
            // PsiInternalMyDsl.g:366:4: (lv_input_3_0= ruleEString )
            {
            // PsiInternalMyDsl.g:366:4: (lv_input_3_0= ruleEString )
            // PsiInternalMyDsl.g:367:5: lv_input_3_0= ruleEString
            {

            					markComposite(elementTypeProvider.getTransition_InputEStringParserRuleCall_3_0ElementType());
            				
            pushFollow(FOLLOW_13);
            lv_input_3_0=ruleEString();

            state._fsp--;


            					doneComposite();
            					if(!current) {
            						associateWithSemanticElement();
            						current = true;
            					}
            				

            }


            }

            // PsiInternalMyDsl.g:380:3: (otherlv_4= 'output' ( (lv_output_5_0= ruleEString ) ) )?
            int alt5=2;
            int LA5_0 = input.LA(1);

            if ( (LA5_0==21) ) {
                alt5=1;
            }
            switch (alt5) {
                case 1 :
                    // PsiInternalMyDsl.g:381:4: otherlv_4= 'output' ( (lv_output_5_0= ruleEString ) )
                    {

                    				markLeaf(elementTypeProvider.getTransition_OutputKeyword_4_0ElementType());
                    			
                    otherlv_4=(Token)match(input,21,FOLLOW_3); 

                    				doneLeaf(otherlv_4);
                    			
                    // PsiInternalMyDsl.g:388:4: ( (lv_output_5_0= ruleEString ) )
                    // PsiInternalMyDsl.g:389:5: (lv_output_5_0= ruleEString )
                    {
                    // PsiInternalMyDsl.g:389:5: (lv_output_5_0= ruleEString )
                    // PsiInternalMyDsl.g:390:6: lv_output_5_0= ruleEString
                    {

                    						markComposite(elementTypeProvider.getTransition_OutputEStringParserRuleCall_4_1_0ElementType());
                    					
                    pushFollow(FOLLOW_14);
                    lv_output_5_0=ruleEString();

                    state._fsp--;


                    						doneComposite();
                    						if(!current) {
                    							associateWithSemanticElement();
                    							current = true;
                    						}
                    					

                    }


                    }


                    }
                    break;

            }


            			markLeaf(elementTypeProvider.getTransition_TargetKeyword_5ElementType());
            		
            otherlv_6=(Token)match(input,22,FOLLOW_3); 

            			doneLeaf(otherlv_6);
            		
            // PsiInternalMyDsl.g:411:3: ( ( ruleEString ) )
            // PsiInternalMyDsl.g:412:4: ( ruleEString )
            {
            // PsiInternalMyDsl.g:412:4: ( ruleEString )
            // PsiInternalMyDsl.g:413:5: ruleEString
            {

            					if (!current) {
            						associateWithSemanticElement();
            						current = true;
            					}
            				

            					markComposite(elementTypeProvider.getTransition_TargetStateCrossReference_6_0ElementType());
            				
            pushFollow(FOLLOW_9);
            ruleEString();

            state._fsp--;


            					doneComposite();
            				

            }


            }


            			markLeaf(elementTypeProvider.getTransition_RightCurlyBracketKeyword_7ElementType());
            		
            otherlv_8=(Token)match(input,16,FOLLOW_2); 

            			doneLeaf(otherlv_8);
            		

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleTransition"

    // Delegated rules


 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x0000000000000030L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000000001000L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x0000000000002000L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x0000000000004000L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x0000000000020000L});
    public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x0000000000018000L});
    public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x0000000000010000L});
    public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x0000000000050000L});
    public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x0000000000080000L});
    public static final BitSet FOLLOW_12 = new BitSet(new long[]{0x0000000000100000L});
    public static final BitSet FOLLOW_13 = new BitSet(new long[]{0x0000000000600000L});
    public static final BitSet FOLLOW_14 = new BitSet(new long[]{0x0000000000400000L});

}
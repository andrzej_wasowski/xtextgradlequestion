/**
 */
package mdsebook.fsm.tests;

import junit.textui.TestRunner;

import mdsebook.fsm.FiniteStateMachine;
import mdsebook.fsm.FsmFactory;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Finite State Machine</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class FiniteStateMachineTest extends NamedElementTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(FiniteStateMachineTest.class);
	}

	/**
	 * Constructs a new Finite State Machine test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FiniteStateMachineTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Finite State Machine test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected FiniteStateMachine getFixture() {
		return (FiniteStateMachine)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(FsmFactory.eINSTANCE.createFiniteStateMachine());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //FiniteStateMachineTest
